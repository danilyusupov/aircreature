import org.apache.log4j.Logger;
import org.junit.Before;

import java.io.IOException;
import java.util.Properties;

import static net.sourceforge.jwebunit.junit.JWebUnit.setBaseUrl;

public abstract class GenericWebTest {

    protected static final Logger log = Logger.getLogger(GenericWebTest.class);
    private String propPath = "/test.properties";

    @Before
    public void prepare(){
        setBaseUrl(getBaseUrl());
    }

    protected String getBaseUrl() {
        Properties properties = new Properties();
        try{
            properties.load(getClass().getResourceAsStream(propPath));
            log.info("Successful read base URL from: " + propPath);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return properties.getProperty("web.baseUrl");
    }
}

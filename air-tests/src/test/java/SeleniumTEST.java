import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import static net.sourceforge.jwebunit.junit.JWebUnit.*;

public class SeleniumTEST extends GenericWebTest {

    /**
     * You must check the driver path to geckodriver.exe before using this test. Also You should check the version of
     * firefox browser, to be sure, that gecko driver maintain this browser!
     * To check the supported version of browser go here: https://github.com/mozilla/geckodriver/releases
     */
    private String driverPath = System.getenv("GECKO_DRIVER");
    private WebDriver driver;
    private String homePath = "/home";
    private String companiesPath = "/companies";
    private String contactPath = "/contact";
    private String registrationPath = "/registration";
    private String personalAreaPath = "/personal_area";
    private String myProjectsPath = "/projects";
    private String createProjectsPath = "/createProject";
    private String viewProjectPath = "/view_project";
    private String userName = "Danil";
    private String userPass = "danil";
    private String projectName = "Su-125";
    private String fileName = "Test-file";
    private String fileRef = "link to model";

    /**
     * Optional method, which helps You to see in 'real-time' (not very fast) anything in test.
     * To skip watching test just comment content of this method.
     *
     * @param time to pause thread
     * @throws InterruptedException in cause of thread error
     */
    private void sleep(long time) throws InterruptedException {
//  ######TIME-SWITCHER##########################################
//        Thread.sleep(time);
    }


    @BeforeTest
    public void launchBrowser() {
        log.info("~~~~~~~~~~LAUNCHING BROWSER~~~~~~~~~~");
        System.setProperty("webdriver.gecko.driver", driverPath);
        FirefoxOptions options = new FirefoxOptions();
        options.setBinary(System.getenv("MOZILLA"));//<<--
        driver = new FirefoxDriver(options);
        log.info("Browser successful launched.");
    }

//    @Test
    public void testWebService() throws InterruptedException {
        //Entering homepage
        driver.get(getBaseUrl() + homePath);
        sleep(1500);

        //Trying to login with empty fields
        WebElement submit1 = driver.findElement(By.id("submit-contact"));
        sleep(1500);
        submit1.click();
        sleep(1000);
        driver.get(getBaseUrl() + homePath);

        //Trying to login with wrong pass
        login("password");
        sleep(1000);
        driver.get(getBaseUrl() + homePath);

        //Going to personal area without pass
        driver.get(getBaseUrl() + personalAreaPath);
        sleep(1000);
        driver.get(getBaseUrl() + homePath);

        //Going to companies page
        driver.get(getBaseUrl() + companiesPath);
        sleep(2500);

        //Going to contact page
        driver.get(getBaseUrl() + contactPath);
        sleep(2500);

        //Trying to make registration with empty fields
        registration("");

        //Trying to make registration of existent user
        registration(userName);

        //Success login
        login(userPass);

        //Going to personal area page
        driver.get(getBaseUrl() + personalAreaPath);
        sleep(2500);

        //Going to projects page
        driver.get(getBaseUrl() + myProjectsPath);
        sleep(2500);

        //Creating existent? project
        creatingProject();

        //Going to projects page
        driver.get(getBaseUrl() + myProjectsPath);
        sleep(2500);

        //Entering created project
        WebElement button = driver.findElement(By.id(projectName));
        button.click();
        sleep(2500);

        //Creating file
        WebElement create = driver.findElement(By.id("create"));
        create.click();
        sleep(2500);
        //TODO: fix fail adding file
        addFile();

        //Deleting file
        driver.get(getBaseUrl() + viewProjectPath);
        WebElement delete = driver.findElement(By.id(fileName));
        sleep(1000);
        delete.submit();
        sleep(2500);

        //Logging out
        logout();
    }

    private void login(String userPass) throws InterruptedException {
        driver.get(getBaseUrl() + homePath);
        sleep(1500);
        WebElement name = driver.findElement(By.name("userName"));
        WebElement password = driver.findElement(By.name("userPassword"));
        WebElement submit = driver.findElement(By.id("submit-contact"));
        name.sendKeys(userName);
        sleep(1000);
        password.sendKeys(userPass);
        sleep(1000);
        submit.click();
        sleep(1000);
    }

    private void logout() throws InterruptedException {
        driver.get(getBaseUrl() + "/logout");
        sleep(1000);
    }

    private void registration(String name) throws InterruptedException {
        driver.get(getBaseUrl() + registrationPath);
        sleep(1500);
        WebElement userName = driver.findElement(By.name("userName"));
        WebElement userPass = driver.findElement(By.name("userPassword"));
        WebElement userEmail = driver.findElement(By.name("userEmail"));
        WebElement submit = driver.findElement(By.id("submit-contact"));
        userName.sendKeys(name);
        sleep(1000);
        userPass.sendKeys("pass");
        sleep(1000);
        userEmail.sendKeys("selenium_test");
        sleep(1000);
        submit.click();
        sleep(1000);
    }

    private void creatingProject() throws InterruptedException {
        driver.get(getBaseUrl() + createProjectsPath);
        sleep(1500);
        WebElement projectName = driver.findElement(By.name("projectName"));
        WebElement projectAnnotation = driver.findElement(By.name("projectAnnotation"));
        WebElement aerodynamics = driver.findElement(By.xpath("//input[@value='Aerodynamics']"));
        WebElement submit = driver.findElement(By.id("submit-contact"));
        projectName.sendKeys(this.projectName);
        sleep(1000);
        projectAnnotation.sendKeys("test...");
        sleep(1000);
        aerodynamics.click();
        sleep(1000);
        submit.click();
        sleep(1000);
    }

    private void addFile() throws InterruptedException {
        WebElement fileName = driver.findElement(By.name("fileName"));
        WebElement fileRef = driver.findElement(By.name("fileRef"));
        WebElement submit = driver.findElement(By.id("submit-ref"));
        fileName.sendKeys(this.fileName);
        sleep(1000);
        fileRef.sendKeys(this.fileRef);
        sleep(1000);
        submit.click();
        sleep(1500);
    }

    @AfterTest
    public void closeDriver() {
        if (driver != null) {
//            driver.close();
            driver.quit();
        }
    }

}

import com.gdc.aircreature.dao.hsqldb.DatabaseConnection;
import org.junit.Test;

import java.sql.Connection;

import static org.junit.Assert.assertEquals;

public class ConnectionTest {

    @Test
    public void testConnection() throws Exception {
        try(Connection connection = DatabaseConnection.connect()) {
            assertEquals("PUBLIC", connection.getSchema());
        }
    }
}

package com.gdc.aircreature.dao;

import java.util.List;

/**
 * General interface for all entity's DAOs in this project
 *
 * @param <T> type of entity
 * @param <V> type of entity's ID (Long or Integer)
 * @author Yusupov Danil
 */
public interface GenericDao<T, V> {

    /**
     * Returns {@code T} entity by intake id
     *
     * @param id of required {@code T} entity
     * @return {@code T} entity or {@code null} if such entity does not exists in database
     */
    T get(V id);

    /**
     * Returns {@code List} of all {@code T} entities existent in database
     *
     * @return {@code List<T>} or {@code null} if there is no any of it
     */
    List<T> getAll();

    /**
     * Removes {@code T} entity from database by intake id.
     *
     * @param id of {@code T} entity to delete
     * @return {@code true} if removing was produced successfully or {@code false} if deleting wasn't produced.
     */
    boolean delete(V id);

    /**
     * Saves created or updates existent {@code T} entity into database. If entity is new - it's {@code entityId} is
     * {@code null}.
     *
     * @param entity {@code T} entity to save
     * @return Id of saved or updated entity
     */
    V save(T entity);

    /**
     * Utility method, which returns name of operating table in database. This name is quite necessary for executing
     * any SQL requests & working with database.
     *
     * @return name of table in database, which binds with working entity.
     */
    String getTableName();

}

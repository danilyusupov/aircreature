package com.gdc.aircreature.dao.hsqldb;

import com.gdc.aircreature.dao.GenericDao;
import com.gdc.aircreature.dao.exceptions.HsqldbDaoException;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * Generic class for all entity's DAOs. Needs to avoid copying some code & make it cleaner.
 *
 * @param <T> type of entity inherited from {@code GenericDao}
 * @author Yusupov Danil
 */
public abstract class HsqldbDao<T> implements GenericDao<T, Long>{

    /**
     * Method inherited from {@code GenericDao} & checks which method needs to be invoked. If {@code entityId} is
     * {@code null}, then {@code insert()} method will execute. If {@code entityId} isn't {@code null}, then
     * {@code update()} method will execute.
     *
     * @param entity {@code T} entity to save
     * @return id of saved or updated entity
     */
    @Override
    public Long save(T entity) {
        return (isNew(entity)) ? insert(entity) : update(entity);
    }

    /**
     * Inserts new entity with {@code entityId == null} into database.
     *
     * @param entity {@code T} entity to save
     * @return {@code V} id of inserted entity
     */
    protected abstract Long insert(T entity);

    /**
     * Updates entity with {@code entityId != null} into database.
     *
     * @param entity {@code T} entity to update
     * @return {@code V} id of updated entity
     */
    protected abstract Long update(T entity);

    /**
     * Checks nullable of entity's ID.
     * @param entity target to check
     * @return (1) {@code true} if ID of entity is {@code null}, (2) {@code false} if ID of entity is {@code not null}
     */
    protected abstract boolean isNew(T entity);
}

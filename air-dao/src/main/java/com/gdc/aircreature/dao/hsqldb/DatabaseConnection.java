package com.gdc.aircreature.dao.hsqldb;

import com.gdc.aircreature.dao.exceptions.ConnectionToDbException;
import com.gdc.aircreature.dao.exceptions.HsqldbDaoException;
import com.gdc.aircreature.dao.logger.DaoLogger;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

/**
 * Provides connection to HSQL database. All connection properties such as: URL, user name & password, - takes from
 * file "/db.properties".
 *
 * @author Yusupov D.
 */
public class DatabaseConnection implements DaoLogger{

    private static String propPath = "/db.properties";
    private static String url;
    private static String userName;
    private static String password;

    /**
     * Connecting server to Hsql database. Before connection initiates inner properties {@code url}, {@code userName} &
     * {@code password}.
     *
     * @return {@code Connection} to HSQL database with used properties
     * @throws ConnectionToDbException (1) if file in path "/db.properties" not found, (2) if "org.hsqldb.jdbc.JDBCDriver"
     * class not found, (3) SQL error.
     */
    public static Connection connect() {
        initParameters();
        try {
            return DriverManager.getConnection(url, userName, password);
        } catch (SQLException e) {
            throw new ConnectionToDbException("Cannot connect to database: '" + url + "' using name: '" + userName +
                    "' and password: '" + password + "'", e);
        }
    }

    /**
     * Gets reference properties from file /db.properties" if they aren't used yet.
     */
    private static void initParameters() {
        if (url == null || userName == null || password ==null) {
            log.info("Some of login parameters are empty. Initiating parameters...");
            try {
                Class.forName("org.hsqldb.jdbc.JDBCDriver");
                Properties properties = new Properties();
                properties.load(DatabaseConnection.class.getResourceAsStream(propPath));
                url = properties.getProperty("dao.url");
                userName = properties.getProperty("dao.username");
                password = properties.getProperty("dao.password");
                log.info("Initiated: URL: " + url + "; user name: '" + userName + "'; password: '" + password + "'");
            } catch (IOException e) {
                throw new ConnectionToDbException("File '" + propPath + "' not found", e);
            } catch (ClassNotFoundException e) {
                throw new ConnectionToDbException("JDBCDriver class not found in local repository:", e);
            }
        }
    }

}

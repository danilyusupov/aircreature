package com.gdc.aircreature.dao.hsqldb;

import com.gdc.aircreature.dao.exceptions.HsqldbDaoException;
import com.gdc.aircreature.dao.logger.DaoLogger;
import com.gdc.aircreature.model.File;

import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class HsqldbFileDaoBeta extends HsqldbDao<File> implements DaoLogger {

    private String tableName;

    public HsqldbFileDaoBeta() {
        this.tableName = getTableName();
    }

    public HsqldbFileDaoBeta(String tableName) {
        this.tableName = tableName;
    }

    public List<File> getAll(Long projectId) {
        List<File> filesList = new ArrayList<>();
        try (Connection connection = DatabaseConnection.connect()) {
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM " + tableName +
                    " WHERE projectId = ?;");
            statement.setLong(1, projectId);
            ResultSet result = statement.executeQuery();
            while (result.next()) {
                filesList.add(create(result));
            }
            return filesList;
        } catch (SQLException e) {
            throw new HsqldbDaoException("Error getting file's list from table '" + tableName +
                    "'.", e);
        }
    }

    @Override
    protected boolean isNew(File entity) {
        return (entity.getFileId() == null);
    }

    @Override
    protected Long update(File file) {
        try (Connection connection = DatabaseConnection.connect()) {
            Blob myBlob = connection.createBlob();
            myBlob.setBytes(1, file.getFileData());
            log.info("Length of '" + file.getFileName() + "': " + myBlob.length());
            PreparedStatement statement = connection.prepareStatement("UPDATE " + tableName +
                    " SET fileName = ?, fileRef = ?, fileData = ?, fileExtension = ?, contentType = ? WHERE fileId = ?;");
            statement.setString(1, file.getFileName());
            statement.setString(2, file.getFileRef());
            if (file.getFileData() == null) {
                statement.setNull(3, Types.BLOB);
            } else {
                myBlob.setBytes(1, file.getFileData());
                statement.setBlob(3, myBlob);
            }
            statement.setString(4, file.getFileExtension());
            statement.setString(5, file.getContentType());
            statement.setLong(6, file.getFileId());
            int result = statement.executeUpdate();
            if (result > 0) {
                log.info("Updated file '" + file.getFileName() + "' with Id:" + file.getFileId() + " in table '" +
                        tableName + "'");
                return file.getFileId();
            }
            return null;
        } catch (SQLException e) {
            throw new HsqldbDaoException("Cannot update file '" + file.getFileName() + "' with Id: " +
                    file.getFileId() + " in project '" + tableName + "'.");
        }
    }

    @Override
    protected Long insert(File file) {
        try (Connection connection = DatabaseConnection.connect()) {
            Blob myBlob = connection.createBlob();
            log.info("Length of '" + file.getFileName() + "': " + myBlob.length());
            PreparedStatement statement = connection.prepareStatement("INSERT INTO " + tableName
                            + " (projectId, fileName, fileRef, fileData, fileExtension, contentType) VALUES(?, ?, ?, ?, ?, ?);",
                    Statement.RETURN_GENERATED_KEYS);
            statement.setLong(1, file.getProjectId());
            statement.setString(2, file.getFileName());
            statement.setString(3, file.getFileRef());
            if (file.getFileData() == null) {
                statement.setNull(4, Types.BLOB);
            } else {
                myBlob.setBytes(1, file.getFileData());
                statement.setBlob(4, myBlob);
            }
            statement.setString(5, file.getFileExtension());
            statement.setString(6, file.getContentType());
            statement.executeUpdate();
            ResultSet result = statement.getGeneratedKeys();
            if (result.next()) {
                long fileId = result.getLong(1);
                log.info("Created file '" + file.getFileName() + " in project with id: " + file.getProjectId());
                return fileId;
            }
            return null;
        } catch (SQLException e) {
            throw new HsqldbDaoException("Error creating file '" + file.getFileName() + "' in project with id " +
                    file.getProjectId(), e);
        }
    }


    @Override
    public boolean delete(Long fileId) {
        try (Connection connection = DatabaseConnection.connect()) {
            PreparedStatement statement = connection.prepareStatement("DELETE FROM " +
                    tableName + " WHERE fileId = ?;");
            statement.setLong(1, fileId);
            return statement.executeUpdate() > 0;
        } catch (SQLException e) {
            throw new HsqldbDaoException("Error deleting file with id:" + fileId + " from table '"
                    + tableName + "'", e);
        }
    }

    @Override
    public File get(Long fileId) {
        try (Connection connection = DatabaseConnection.connect()) {
            PreparedStatement statement = connection.prepareStatement(
                    "SELECT * FROM " + tableName + " WHERE fileId = ?;");
            statement.setLong(1, fileId);
            ResultSet result = statement.executeQuery();
            return result.next() ? create(result) : null;
        } catch (SQLException e) {
            throw new HsqldbDaoException("Error getting file with id:" + fileId + " from table '"
                    + tableName + "'", e);
        }
    }

    public File get(Long projectId, String fileName) {
        try (Connection connection = DatabaseConnection.connect()) {
            PreparedStatement statement = connection.prepareStatement(
                    "SELECT * FROM " + tableName + " WHERE projectId = ? AND fileName = ?;");
            statement.setLong(1, projectId);
            statement.setString(2, fileName);
            ResultSet result = statement.executeQuery();
            return result.next() ? create(result) : null;
        } catch (SQLException e) {
            throw new HsqldbDaoException("Error getting file '" + fileName + "' from table '"
                    + tableName + "'", e);
        }
    }

    private File create(ResultSet result) throws SQLException {
        Long fileId = result.getLong("fileId");
        Long projectId = result.getLong("projectId");
        String fileName = result.getString("fileName");
        String fileRef = result.getString("fileRef");
        Blob myBlob = result.getBlob("fileData");
        String fileExtension = result.getString("fileExtension");
        String contentType = result.getString("contentType");
        if (myBlob != null) {
            int blobLength = (int) myBlob.length();
            byte[] fileData = myBlob.getBytes(1, blobLength);
            return new File(fileId, projectId, fileName, fileData, fileExtension, contentType);
        }
        return new File(fileId, projectId, fileName, fileRef);
    }

    @Override
    public String getTableName() {
        Properties properties = new Properties();
        try {
            properties.load(getClass().getResourceAsStream("/db.properties"));
            return properties.getProperty("dao.file.tablename");
        } catch (IOException e) {
            throw new HsqldbDaoException("Cannot read table name from 'dao.project.tablename' " +
                    "from file db.properties", e);
        }
    }

    @Deprecated
    @Override
    public List<File> getAll() {
        return null;
    }
}

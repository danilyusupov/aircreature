package com.gdc.aircreature.dao.hsqldb;

import com.gdc.aircreature.dao.exceptions.HsqldbDaoException;
import com.gdc.aircreature.dao.logger.DaoLogger;
import com.gdc.aircreature.model.Project;
import com.gdc.aircreature.model.ProjectType;

import java.io.*;
import java.sql.*;
import java.util.*;

public class HsqldbProjectDao extends HsqldbDao<Project> implements DaoLogger {

    private String tableName;

    public HsqldbProjectDao() {
        this.tableName = getTableName();
    }

    public HsqldbProjectDao(String tableName) {
        this.tableName = tableName;
    }

    @Override
    public Project get(Long id) {
        try (Connection connection = DatabaseConnection.connect()) {
            PreparedStatement statement = connection.prepareStatement(
                    "SELECT * FROM " + tableName + " WHERE projectId = ?;");
            statement.setLong(1, id);
            ResultSet result = statement.executeQuery();
            return result.next() ? create(result) : null;
        } catch (SQLException e) {
            throw new HsqldbDaoException("Error getting project with id:" + id, e);
        }
    }

    @Override
    public List<Project> getAll() {
        List<Project> projectsList = new ArrayList<>();
        try (Connection connection = DatabaseConnection.connect()) {
            Statement statement = connection.createStatement();
            ResultSet result = statement.executeQuery(
                    "SELECT * FROM " + tableName + ";");
            while (result.next()) {
                projectsList.add(create(result));
            }
            return projectsList;
        } catch (SQLException e) {
            throw new HsqldbDaoException("Error getting project's list from table '" + tableName, e);
        }
    }

    @Override
    public boolean delete(Long id) {
        if (!isExistent(id)) {
            log.error("Cannot delete project: no such project with id:" + id);
            return false;
        }
        try (Connection connection = DatabaseConnection.connect()) {
            PreparedStatement statement = connection.prepareStatement("DELETE FROM " + tableName +
                    " WHERE projectId = ?;");
            statement.setLong(1, id);
            return statement.executeUpdate() > 0;
        } catch (SQLException e) {
            throw new HsqldbDaoException("Error deleting project with id:" + id, e);
        }
    }

    @Override
    protected Long update(Project entity) {
        try (Connection connection = DatabaseConnection.connect()) {
            Clob clob = connection.createClob();
            clob.setString(1, entity.getDescription());
            PreparedStatement statement = connection.prepareStatement(
                    "UPDATE " + tableName + " SET projectName = ?, projectAuthor = ?, projectType = ?, description = ?" +
                            " WHERE projectId = ?;");
            statement.setString(1, entity.getProjectName());
            statement.setLong(2, entity.getProjectAuthor());
            statement.setString(3, entity.getProjectType().toString());
            statement.setString(4, entity.getDescription());
            statement.setLong(5, entity.getProjectId());
            int result = statement.executeUpdate();
            if (result > 0) {
                log.info("Updated project '" + entity.getProjectName() + "' with id:" + entity.getProjectId());
                return entity.getProjectId();
            }
        } catch (SQLException e) {
            throw new HsqldbDaoException("Error updating project '" + entity.getProjectName() +
                    "' with id:" + entity.getProjectId());
        }
        log.warn("Cannot return ID of project: '" + entity.getProjectName() + "'.");
        return null;
    }

    @Override
    protected boolean isNew(Project entity) {
        return (entity.getProjectId() == null);
    }

    @Override
    protected Long insert(Project entity) {
        try (Connection connection = DatabaseConnection.connect()) {
            Clob clob = connection.createClob();
            PreparedStatement statement = connection.prepareStatement("INSERT INTO " + tableName +
                    " VALUES (null, ?, ?, ?, ?);", Statement.RETURN_GENERATED_KEYS);
            statement.setString(1, entity.getProjectName());
            statement.setLong(2, entity.getProjectAuthor());
            statement.setString(3, entity.getProjectType().toString());
            statement.setString(4, entity.getDescription());
            statement.executeUpdate();
            ResultSet result = statement.getGeneratedKeys();
            if (result.next()) {
                long projectId = result.getLong(1);
                log.info("Created project '" + entity.getProjectName() + " by user with id: " + entity.getProjectAuthor());
                return projectId;
            }
        } catch (SQLException e) {
            throw new HsqldbDaoException("Error creating project '" + entity.getProjectName() + "'.", e);
        }
        log.warn("Cannot return ID of project: '" + entity.getProjectName() + "'.");
        return null;
    }

    @Override
    public String getTableName() {
        Properties properties = new Properties();
        try {
            properties.load(getClass().getResourceAsStream("/db.properties"));
            return properties.getProperty("dao.project.tablename");
        } catch (IOException e) {
            throw new HsqldbDaoException("Cannot read table name from 'dao.project.tablename' " +
                    "from file db.properties", e);
        }
    }

    public Project get(String name) {
        try (Connection connection = DatabaseConnection.connect()) {
            PreparedStatement statement = connection.prepareStatement(
                    "SELECT * FROM " + tableName + " WHERE projectName = ?;");
            statement.setString(1, name);
            ResultSet result = statement.executeQuery();
            return result.next() ? create(result) : null;
        } catch (SQLException e) {
            throw new HsqldbDaoException("Error getting project '" + name, e);
        }
    }

    public List<Project> getByAuthor(Long id) {
        List<Project> projectsList = new ArrayList<Project>();
        try (Connection connection = DatabaseConnection.connect()) {
            PreparedStatement statement = connection.prepareStatement(
                    "SELECT * FROM " + tableName + " WHERE projectAuthor = ?;");
            statement.setLong(1, id);
            ResultSet result = statement.executeQuery();
            while (result.next()) {
                projectsList.add(create(result));
            }
            return projectsList;
        } catch (SQLException e) {
            throw new HsqldbDaoException("Error getting project's list from table '" + tableName, e);
        }
    }

    private Project create(ResultSet result) throws SQLException {
        long projectId = result.getLong("projectId");
        String projectName = result.getString("projectName");
        long projectAuthor = result.getLong("projectAuthor");
        ProjectType projectType = ProjectType.valueOf(result.getString("projectType"));
        String description = result.getString("description");
        return new Project(projectId, projectName, projectAuthor, projectType, description);
    }

    private boolean isExistent(Long id) {
        Set<Long> idSet = new TreeSet<>();
        for (Project project : getAll()) {
            idSet.add(project.getProjectId());
        }
        return idSet.contains(id);
    }
}

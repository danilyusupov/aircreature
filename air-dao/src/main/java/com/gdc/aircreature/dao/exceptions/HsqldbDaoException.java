package com.gdc.aircreature.dao.exceptions;

public class HsqldbDaoException extends RuntimeException {

    public HsqldbDaoException(String s) {
        super(s);
    }

    public HsqldbDaoException(String s, Throwable throwable) {
        super(s, throwable);
    }
}

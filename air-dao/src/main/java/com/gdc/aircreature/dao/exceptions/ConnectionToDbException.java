package com.gdc.aircreature.dao.exceptions;

public class ConnectionToDbException extends RuntimeException{

    public ConnectionToDbException(String s) {
        super(s);
    }

    public ConnectionToDbException(String s, Throwable throwable) {
        super(s, throwable);
    }

}

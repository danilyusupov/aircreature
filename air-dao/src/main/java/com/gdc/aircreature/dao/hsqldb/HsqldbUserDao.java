package com.gdc.aircreature.dao.hsqldb;

import com.gdc.aircreature.dao.exceptions.HsqldbDaoException;
import com.gdc.aircreature.dao.logger.DaoLogger;
import com.gdc.aircreature.model.Status;
import com.gdc.aircreature.model.User;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class HsqldbUserDao extends HsqldbDao<User> implements DaoLogger {

    private String tableName;

    public HsqldbUserDao() {
        this.tableName = getTableName();
    }

    public HsqldbUserDao(String tableName) {
        this.tableName = tableName;
    }

    public User get(String userName) {
        try (Connection connection = DatabaseConnection.connect()) {
            PreparedStatement statement = connection.prepareStatement(
                    "SELECT * FROM " + tableName + " WHERE userName = ?;");
            statement.setString(1, userName);
            ResultSet result = statement.executeQuery();
            return result.next() ? create(result) : null;
        } catch (SQLException e) {
            throw new HsqldbDaoException("There is a problem with connection, or with query preparation, or there is" +
                    " no account with name: '" + userName + "' in table: '" + tableName + "'", e);
        }
    }

    @Override
    public User get(Long id) {

        try (Connection connection = DatabaseConnection.connect()) {
            PreparedStatement statement = connection.prepareStatement(
                    "SELECT * FROM " + tableName + " WHERE userId = ?;");
            statement.setLong(1, id);
            ResultSet result = statement.executeQuery();
            return result.next() ? create(result) : null;
        } catch (SQLException e) {
            throw new HsqldbDaoException("Error getting user with ID=" + id, e);
        }

    }

    @Override
    public List<User> getAll() {
        List<User> userList = new ArrayList<>();

        try (Connection connection = DatabaseConnection.connect()) {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(
                    "SELECT * FROM " + tableName + " ;");
            while (resultSet.next()) {
                userList.add(create(resultSet));
            }
            return userList.isEmpty() ? null : userList;
        } catch (SQLException e) {
            throw new HsqldbDaoException("Cannot get all users in table '" + tableName + "':", e);
        }
    }

    @Override
    public boolean delete(Long id) {
        try (Connection connection = DatabaseConnection.connect()) {
            PreparedStatement statement = connection.prepareStatement(
                    "DELETE FROM " + tableName + " WHERE userId = ?;");
            statement.setLong(1, id);
            return statement.executeUpdate() > 0;

        } catch (SQLException e) {
            throw new HsqldbDaoException("Error removing entity with ID = " + id, e);
        }
    }

    @Override
    protected Long insert(User entity) {
        try (Connection connection = DatabaseConnection.connect()) {
            PreparedStatement statement = connection.prepareStatement("INSERT INTO " + tableName +
                    " VALUES (null, ?, ?, ?, ?);", Statement.RETURN_GENERATED_KEYS);
            statement.setString(1, entity.getUserName());
            statement.setString(2, entity.getUserPassword());
            statement.setString(3, entity.getUserStatus().toString());
            statement.setString(4, entity.getUserEmail());
            statement.executeUpdate();
            ResultSet result = statement.getGeneratedKeys();
            if (result.next()) {
                long userId = result.getLong(1);
                log.info("Created user with id: " + userId);
                return userId;
            }
        } catch (SQLException e) {
            throw new HsqldbDaoException("Cannot create user '" + entity.getUserName() + "'");
        }
        log.warn("Cannot return ID of user: '" + entity.getUserName() + "'.");
        return null;
    }

    @Override
    protected Long update(User entity) {
        try (Connection connection = DatabaseConnection.connect()) {
            PreparedStatement statement = connection.prepareStatement("UPDATE " + tableName +
                    " SET userName = ?, userPassword = ?, userStatus = ?, userEmail = ? WHERE userId = ?;");
            statement.setString(1, entity.getUserName());
            statement.setString(2, entity.getUserPassword());
            statement.setString(3, entity.getUserStatus().toString());
            statement.setString(4, entity.getUserEmail());
            statement.setLong(5, entity.getUserId());
            int result = statement.executeUpdate();
            if (result > 0) {
                log.info("Updated user '" + entity.getUserName() + "' with Id:" + entity.getUserId());
                return entity.getUserId();
            }
        } catch (SQLException e) {
            throw new HsqldbDaoException("Cannot update user '" + entity.getUserName() + "' with Id: " +
                    entity.getUserId() + " for table '" + tableName + "'.");
        }
        log.warn("Cannot return ID of user: '" + entity.getUserName() + "'.");
        return null;
    }

    @Override
    protected boolean isNew(User entity) {
        return (entity.getUserId() == null);
    }

    @Override
    public String getTableName() {
        Properties properties = new Properties();
        try {
            properties.load(getClass().getResourceAsStream("/db.properties"));
            return properties.getProperty("dao.user.tablename");
        } catch (IOException e) {
            throw new HsqldbDaoException("Cannot read table name from 'dao.project.tablename " +
                    "from file db.properties", e);
        }
    }

    private User create(ResultSet result) throws SQLException {
        long userId = result.getLong("userId");
        String userName = result.getString("userName");
        String userPassword = result.getString("userPassword");
        Status userStatus = Status.valueOf(result.getString("userStatus"));
        String userEmail = result.getString("userEmail");
        return new User(userId, userName, userPassword, userStatus, userEmail);
    }

}

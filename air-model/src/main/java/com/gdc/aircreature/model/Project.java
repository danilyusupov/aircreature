package com.gdc.aircreature.model;

/**
 * Consists information about user's inventions in aircraft design field. Contains stack of {@code File}.
 *
 * @author Yusupov D.
 */
public class Project {

    /** {@code projectId} is {@code NOT NULL} & {@code PRIMARY KEY} */
    private Long projectId;

    /** {@code projectName} is {@code UNIQUE} value */
    private String projectName;

    /** {@code userId} of initiator */
    private Long projectAuthor;


    /** {@code projectType} describes type of current project. It helps to company representative to sort projects.
     * Also it helps to developers find each other to communicate & cooperate.
     */
    private ProjectType projectType;

    /**
     * Annotation to project
     */
    private String description;

    public Project(Long projectId, String projectName, Long projectAuthor, ProjectType projectType, String description) {
        this.projectId = projectId;
        this.projectName = projectName;
        this.projectAuthor = projectAuthor;
        this.projectType = projectType;
        this.description = description;
    }

    public Project(String projectName, Long projectAuthor, ProjectType projectType, String description) {
        this.projectName = projectName;
        this.projectAuthor = projectAuthor;
        this.projectType = projectType;
        this.description = description;
    }

    public Long getProjectId() {
        return projectId;
    }

    public String getProjectName() {
        return projectName;
    }

    public Long  getProjectAuthor() {
        return projectAuthor;
    }

    public ProjectType getProjectType() {
        return projectType;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getDescription() {
        return description;
    }
}

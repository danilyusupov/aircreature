package com.gdc.aircreature.model;

/**
 * Enum for initializing type of user's project
 */
public enum ProjectType {
    Aerodynamics, Structure, Design, Technologic, Acoustic, Aeroelastic
}

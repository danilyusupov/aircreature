package com.gdc.aircreature.model;

/**
 * An entity, which personifies user of provided service.
 *
 * @author Danil Yusupov
 */
public class User {

    /** {@code userId} is {@code NOT NULL} & {@code PRIMARY KEY} */
    private Long userId;

    /** {@code userName} is {@code UNIQUE} value */
    private String userName;

    /** Contains user's password and must be hashed while keeping it in database */
    private String userPassword;

    /** Shows user's belonging to a company or that he is part of development */
    private Status userStatus;
    private String userEmail;

    public User() {
    }

    public User(Long userId, String userName, String userPassword, Status userStatus, String userEmail) {
        this.userId = userId;
        this.userName = userName;
        this.userPassword = userPassword;
        this.userStatus = userStatus;
        this.userEmail = userEmail;
    }

    public User(String userName, String userPassword, Status userStatus, String userEmail) {
        this.userName = userName;
        this.userPassword = userPassword;
        this.userStatus = userStatus;
        this.userEmail = userEmail;
    }

    public Long getUserId() {
        return userId;
    }

    public String getUserName() {
        return userName;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public Status getUserStatus() {
        return userStatus;
    }
}

package com.gdc.aircreature.model;

/**
 * Is a special group of users, which unites them by similar interests or {@code Project}.
 * Frozen till sometime...
 *
 * @author Yusupov D.
 */
public class Group {

    /** groupId is {@code NOT NULL} & {@code PRIMARY KEY} */
    private long groupId;

    /** groupName is {@code UNIQUE} value */
    private String groupName;

    private String groupDescription;

    public Group(String groupName) {
        this.groupName = groupName;
    }

    public long getGroupId() {
        return groupId;
    }

    public String getGroupName() {
        return groupName;
    }

    public String getGroupDescription() {
        return groupDescription;
    }

    public void setGroupDescription(String groupDescription) {
        this.groupDescription = groupDescription;
    }
}

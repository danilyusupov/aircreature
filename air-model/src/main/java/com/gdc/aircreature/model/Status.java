package com.gdc.aircreature.model;

/**
 * Enum for classify users by status.
 */
public enum Status {
    Developer, Team_Leader, Representative
}

package com.gdc.aircreature.model;

import java.sql.Blob;

/**
 * An entity, which describes user's file added into {@code Project}
 *
 * @author Yusupov D.
 */
public class File {
    /**
     * {@code NOT NULL}, {@code PRIMARY KEY} and auto generated in database. Starts with 1.
     */
    private Long fileId;
    /**
     * Identifier of project owner of this file.
     */
    private Long projectId;
    private String fileName;
    /**
     * Can be {@code null}
     */
    private String fileRef;

    /**
     * Both can be {@code null}
     */
    private byte[] fileData;
    private String fileExtension;
    private String contentType;

    public File(Long fileId, Long projectId, String fileName, String fileRef) {
        this.fileId = fileId;
        this.projectId = projectId;
        this.fileName = fileName;
        this.fileRef = fileRef;
    }

    public File(Long fileId, Long projectId, String fileName, byte[] fileData, String fileExtension, String contentType) {
        this.fileId = fileId;
        this.projectId = projectId;
        this.fileName = fileName;
        this.fileData = fileData;
        this.fileExtension = fileExtension;
        this.contentType = contentType;
    }

    public File(Long projectId, String fileName, String fileRef) {
        this.projectId = projectId;
        this.fileName = fileName;
        this.fileRef = fileRef;
    }

    public File(Long projectId, String fileName, byte[] fileData, String fileExtension, String contentType) {
        this.projectId = projectId;
        this.fileName = fileName;
        this.fileData = fileData;
        this.fileExtension = fileExtension;
        this.contentType = contentType;
    }

    public Long getFileId() {
        return fileId;
    }

    public String getFileName() {
        return fileName;
    }

    public String getFileRef() {
        return fileRef;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileExtension() {
        return fileExtension;
    }

    public byte[] getFileData() {
        return fileData;
    }

    public Long getProjectId() {
        return projectId;
    }

    public String getContentType() {
        return contentType;
    }
}

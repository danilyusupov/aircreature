<!DOCTYPE html>
<%@ page pageEncoding="UTF-8" %>
<html lang="en">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <title>Project view</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Loading: Normalize, Grid and Styles -->
    <link rel="stylesheet" href="assets/css/normalize.css" media="screen">
    <link rel="stylesheet" href="assets/css/grid.css" media="screen">
    <link rel="stylesheet" href="assets/css/style.css" media="screen">

    <!-- Loading: Font Awesome -->
    <link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css">
    <!--[if IE 7]>
    <link rel="stylesheet" href="assets/font-awesome/css/font-awesome-ie7.min.css">
    <![endif]-->

    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

</head>
<body>
<% if (request.getSession().getAttribute("fileMessage") != null) { %>
<div class="fix">
    <p align="center"><h3><%=request.getSession().getAttribute("fileMessage")%></h3></p>
</div>
<% } %>

<!-- Start: Intro -->
<section id="intro" class="fx-backstretch">
    <nav id="nav">
        <ul class="clearfix">
            <li><a href="/home">Home</a></li>
            <li><a href="/personal_area">Personal Area</a></li>
            <li><a href="/projects">Projects</a></li>
            <li><a href="/companies">Companies</a></li>
            <li><a href="/contact">Contact</a></li>
            <li><a href="/logout">Logout (
                            <%=request.getSession().getAttribute("userName")%>
                                                       )</a></li>

        </ul>
    </nav>
</section>
<!-- End: Intro -->

<!-- Start: Personal profile -->
<section id="profile" class="section">
    <div class="container">
        <div class="row">
            <div class="col-full">
                <h2 class="section-title"><%=request.getSession().getAttribute("projectName")%></h2>
                <div class="centered line"></div>
            </div>
        </div>

        <div class="row">
            <a href="/projects" class="button button-medium button-submit">BACK TO PROJECTS</a>
        </div>

        <div class="row section-content wel-sec">
                    <div class="col-1-3">
                        <h4>Project info</h4>

                        <p>Name: <%=request.getSession().getAttribute("projectName")%></p>
                        <p>Author: <%=request.getSession().getAttribute("projectAuthor")%></p>
                        <p>Type: <%=request.getSession().getAttribute("projectType")%></p>
                        <p>Annotation: <%=request.getSession().getAttribute("description")%></p>

                    </div>

                </div>

        <%@ page import="com.gdc.aircreature.model.File" %>

        <div class="job">
                <h4>Files:</h4>
                <ul class="ul-list wel-sec">
                    <%  File[] fileList = (File[]) request.getSession().getAttribute("fileList");
                        for (File file : fileList) { %>
                        <li><form id="delete" name="delete" method="post">
                            <div class="col-1-2 job-company"><%=file.getFileName()%></div>
                            <% if (file.getFileRef() != null) { %>
                                    <a href=<%=file.getFileRef()%>><%=file.getFileName()%></a>
                            <% } else { request.getSession().setAttribute("fileId", file.getFileId()); %>
                                <a href="/download" class="button button-medium button-submit upsix">Download</a>
                            <% } %>
                                <input type="text" name="select" value="<%=file.getFileName()%>" class="hidden">
                                <input type="submit" id="submit" class="button button-medium button-submit upsix" value="Delete">
                        </li></form></br>
                    <% } %>
                </ul>
                <form action="/add_file">
                   <input type="submit" class="button button-medium button-submit" value="Create file..." id="create" />
                </form>
        </div>
    </div>
</section>
<!-- End: Personal profile -->

<footer>
    <div class="container">
        2018 created by Danil Yusupov for educational purposes.
    </div>
</footer>

</body>
</html>
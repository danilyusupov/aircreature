<%@ page language="java" contentType="text/html; charset=UTF-8"
pageEncoding="UTF-8"%>
<html lang="en">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <title>Personal Area</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Loading: Normalize, Grid and Styles -->
    <link rel="stylesheet" href="assets/css/normalize.css" media="screen">
    <link rel="stylesheet" href="assets/css/grid.css" media="screen">
    <link rel="stylesheet" href="assets/css/style.css" media="screen">

    <!-- Loading: Font Awesome -->
    <link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css">
    <!--[if IE 7]>
    <link rel="stylesheet" href="assets/font-awesome/css/font-awesome-ie7.min.css">
    <![endif]-->

    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>
<body>

<!-- Start: Intro -->
<section id="intro" class="fx-backstretch">
    <nav id="nav">
        <ul class="clearfix">
            <li><a href="/home">Home</a></li>
            <li><a href="/personal_area">Personal Area</a></li>
            <li><a href="/companies">Companies</a></li>
            <li><a href="/about">About</a></li>
            <li><a href="/contact">Contact</a></li>
            <li><a href="/logout">Logout (
                            <%=request.getSession().getAttribute("userName")%>
                                                       )</a></li>

        </ul>
    </nav>
</section>
<!-- End: Intro -->

<!-- Start: Personal profile -->
<section id="profile" class="section">
    <div class="container">
        <% if (request.getSession().getAttribute("registrationErrorMessage") != null) { %>
        <p align="center"><h3><font color="red"><%=request.getSession().getAttribute("projectSuccess")%></font></h3></p>
        <% } %>
        <div class="row">
            <div class="col-full">
                <h2 class="section-title">My projects</h2>
                <div class="centered line"></div>
            </div>
        </div>

        <div class="col-1-3">
            <p><h4>Current projects:</h4></p>
                <%@ page import="com.gdc.aircreature.model.Project" %>
           <form method="post">
           <ul>
                <%  Project[] projectList = (Project[]) request.getSession().getAttribute("projectList");
                    for (Project project : projectList) { %>
                    <li>
                        <form method="post" id="<%=project.getProjectName()%>">
                            <input type="hidden" name="select" value="<%=project.getProjectName()%>"/>
                            <button class="button button-medium button-submit" id="<%=project.getProjectName()%>"><%=project.getProjectName()%></button>
                        </form>
                    </li></br>
                <% } %>
            </ul>
            </form>

            </br>
            <a href="/createProject" class="button button-medium button-submit">Create project...</a>
        </div>

    </div>
</section>
<!-- End: Personal profile -->

<footer>
    <div class="container">
        2018 created by Danil Yusupov for educational purposes.
    </div>
</footer>

</body>
</html>
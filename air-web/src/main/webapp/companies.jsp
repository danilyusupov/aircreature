<!DOCTYPE html>
<%@ page pageEncoding="UTF-8" %>
<html>

<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <title>Companies</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Loading: Normalize, Grid and Styles -->
    <link rel="stylesheet" href="assets/css/normalize.css" media="screen">
    <link rel="stylesheet" href="assets/css/grid.css" media="screen">
    <link rel="stylesheet" href="assets/css/style-companies.css" media="screen">

    <!-- Loading: Font Awesome -->
    <link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css">
    <!--[if IE 7]>
    <link rel="stylesheet" href="assets/font-awesome/css/font-awesome-ie7.min.css">
    <![endif]-->

    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>

<body>

<!-- Start: Intro -->
<section id="intro" class="fx-backstretch">
    <nav id="nav">
        <ul class="clearfix">
            <li><a href="/home">Home</a></li>
            <li><a href="/personal_area">Personal Area</a></li>
            <li><a href="/registration">Registration</a></li>
            <li><a href="/contact">Contact</a></li>
            <% if (request.getSession().getAttribute("userName") != null) {
                            %><li><a href="/logout">Logout (
                                                            <%=request.getSession().getAttribute("userName")%>
                                                            )</a></li>
                            <% } %>
        </ul>
    </nav>
</section>
<!-- End: Intro -->

<!-- Start: Personal profile -->
<section id="profile" class="section">
    <div class="container">
        <div class="row">
            <div class="col-full">
                <h2 class="section-title">Companies</h2>
                <div class="centered line"></div>
            </div>
        </div>

        <div class="row section-content wel-sec">
            <div class="row section-content wel-sec">
                <div class="col-1-3 job-project">
                    <a href="http://www.sukhoi.org/"><img src="assets/img/su.png"></a>

                    <p>ПАО «Компания «Сухой» — крупнейший российский авиационный холдинг с числом работников более 25
                        тыс.
                        человек.</p>

                </div>
                <div class="col-1-3 job-project">
                    <a href="https://kai.ru/"><img src="assets/img/kai.png"></a>

                    <p>Казанский национальный исследовательский технический университет имени А. Н. Туполева.</p>
                </div>
                <div class="col-1-3 job-project">
                    <a href="http://www.tupolev.ru/"><img src="assets/img/tu.png"></a>

                    <p>ПАО «Туполев» – крупнейший разработчик авиационной техники, занимающийся проектированием,
                        производством и испытаниями летательных аппаратов различного назначения.</p>
                </div>
            </div>
            <br/>
            <div class="row section-content wel-sec">
                <div class="col-1-3 job-project">
                    <a href="http://www.tsagi.ru/"><img src="assets/img/tsagi.png"></a>

                    <p>Центральный аэрогидродинамический институт им. профессора Н. Е. Жуковского — крупнейший
                        государственный научный авиационный центр России.</p>
                </div>
                <div class="col-1-3 job-project">
                    <a href="http://www.uacrussia.ru/ru/"><img src="assets/img/oak.png"></a>

                    <p>ПАО «Объединённая авиастрои́тельная корпора́ция» — российское публичное акционерное общество,
                        объединяющее крупнейшие авиастроительные предприятия России.</p>
                </div>
                <div class="col-1-3 job-project">
                    <a href="https://www.roscosmos.ru/"><img src="assets/img/kosmos.png"></a>

                    <p>Государственная корпорация по космической деятельности «Роскосмос» — российская корпорация,
                        управляющая космической отраслью страны, созданная путём преобразования Федерального
                        космического агентства «Роскосмос».</p>
                </div>
            </div>
            <br/>
            <div class="row section-content wel-sec">
                <div class="col-1-3 job-project">
                    <a href="www.russianhelicopters.aero/"><img src="assets/img/helicopters.png"></a>

                    <p>АО «Вертолёты России» — российский вертолётостроительный холдинг, объединяющий большинство
                        вертолётостроительных предприятий страны.</p>
                </div>
                <div class="col-1-3 job-project">
                    <a href="http://www.ansys.com/"><img src="assets/img/ansys.png"></a>

                    <p>Компания ANSYS, Inc. предлагает широкий спектр программных продуктов для решения инженерных
                        задач с использованием технологий численного моделирования. </p>
                </div>
                <div class="col-1-3 job-project">
                    <a href="http://rostec.ru/"><img src="assets/img/rosteh.png"></a>

                    <p>Ростех — российская государственная корпорация, созданная в конце 2007 года для содействия в
                        разработке, производстве и экспорте высокотехнологичной промышленной продукции гражданского
                        и военного назначения.</p>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End: Personal profile -->


<footer>
    <div class="container">
        2018 created by Danil Yusupov for educational purposes.
    </div>
</footer>

</body>

</html>
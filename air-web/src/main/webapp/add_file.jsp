<!DOCTYPE html>
<%@ page pageEncoding="UTF-8" %>
<html>

<head>

    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <title>Creating new project</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Loading: Normalize, Grid and Styles -->
    <link rel="stylesheet" href="assets/css/normalize.css" media="screen">
    <link rel="stylesheet" href="assets/css/grid.css" media="screen">
    <link rel="stylesheet" href="assets/css/style.css" media="screen">

    <!-- Loading: Font Awesome -->
    <link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css">
    <!--[if IE 7]>
    <link rel="stylesheet" href="assets/font-awesome/css/font-awesome-ie7.min.css">
    <![endif]-->

    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <script type="text/javascript">
        function getExtension() {
            var file = document.getElementById("browse").files[0],
            ext = "not identified",
            parts = file.name.split('.');
            if(parts.length > 1) ext = parts.pop();
            document.getElementById("extension").innerHTML = [ext];
        }

        document.getElementById('uploadInput').addEventListener('change', getExtension);
    </script>

</head>

<body>

<!-- Start: Intro -->
<section id="intro" class="fx-backstretch">
    <nav id="nav">
        <ul class="clearfix">
            <li><a href="/home">Home</a></li>
            <li><a href="/personal_area">Personal Area</a></li>
            <li><a href="/projects">Projects</a></li>
            <li><a href="/companies">Companies</a></li>
            <li><a href="/contact">Contact</a></li>
            <li><a href="/logout">Logout (
                <%=request.getSession().getAttribute("userName")%>
                )</a></li>
        </ul>
    </nav>
</section>
<!-- End: Intro -->

<!-- Start: Personal profile -->
<section id="profile" class="section">
    <div class="container">
        <div class="row">
            <div class="col-full">
                <h2 class="section-title">Adding new file</h2>
                <div class="centered line"></div>
            </div>
        </div>

        <div class="row">
                    <a href="/projects" class="button button-medium button-submit">BACK TO PROJECTS</a>
                </div>

        <p class="row section-content">
            <div class="col-1-1 col-wrap centered text-center">
                <form id="form-ref" name="form-ref" enctype="multipart/form-data" method="post">
                    <div class="col-full">
                        <label for="fileData">Upload Your file:</label>
                        <input type="text" id="extension" name="extension" class="hidden">
                        <a href="#" class="button button-medium button-submit" onclick="document.getElementById('browse').click(); return false;" />Browse...</a>
                        <input type="file" name="fileData" id="browse" value="" class="hidden">
                        <input type="submit" class="button button-medium button-submit" id="upload-file" value="ADD FILE">
                    </div>
                </form>
            </div>
        </p>

        <p><div class="separator"></div></p>
        <div class="text-center col-full">
            <label>Or You can also add link to Your file placed in cloud:</label>
        </div>

        <p class="row section-content">
            <div class="col-1-1 col-wrap centered text-center">
                <div id="form-message"></div>
                <form id="form-contact" name="form-contact" method="post">
                    <div class="col-full">
                        <label for="fileName">File Name:</label>
                        <input type="text" name="fileName" id="fileName" value="">
                    </div>
                    <div class="col-full">
                        <label for="fileRef">Link to file:</label>
                        <input type="text" name="fileRef" id="fileRef" value="">
                    </div>
                    <div class="col-full">
                        <input type="submit" class="button button-medium button-submit" id="submit-ref" value="ADD LINK TO FILE">
                    </div>
                </form>
            </div>

        <p><div class="separator"></div></p>
        <div class="text-center">
            <label>List of some storage services:</label>
        </div>

        <div class="job-attributions">
            <a href="https://mega.nz/" class="button button-medium button-submit">MEGA</a>
            <span class="bullet">&bull;</span>
            <a href="https://www.pcloud.com/" class="button button-medium button-submit">pCloud</a>
            <span class="bullet">&bull;</span>
            <a href="https://www.mediafire.com/upgrade/" class="button button-medium button-submit">MediaFire</a>
            <span class="bullet">&bull;</span>
            <a href="https://www.dropbox.com/individual" class="button button-medium button-submit">Dropbox</a>
        </div>
        </p>

    </div>
</section>
<!-- End: Personal profile -->


<footer>
    <div class="container">
        2018 created by Danil Yusupov for educational purposes.
    </div>
</footer>

</body>

</html>
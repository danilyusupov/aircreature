<!DOCTYPE html>
<%@ page pageEncoding="UTF-8" %>
<html lang="en">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <title>Personal Area</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Loading: Normalize, Grid and Styles -->
    <link rel="stylesheet" href="assets/css/normalize.css" media="screen">
    <link rel="stylesheet" href="assets/css/grid.css" media="screen">
    <link rel="stylesheet" href="assets/css/style.css" media="screen">

    <!-- Loading: Font Awesome -->
    <link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css">
    <!--[if IE 7]>
    <link rel="stylesheet" href="assets/font-awesome/css/font-awesome-ie7.min.css">
    <![endif]-->

    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>
<body>

<!-- Start: Intro -->
<section id="intro" class="fx-backstretch">
    <nav id="nav">
        <ul class="clearfix">
            <li><a href="/home">Home</a></li>
            <li><a href="/companies">Companies</a></li>
            <li><a href="/about">About</a></li>
            <li><a href="/contact">Contact</a></li>
            <li><a href="/logout">Logout (
                            <%=request.getSession().getAttribute("userName")%>
                                                       )</a></li>

        </ul>
    </nav>
</section>
<!-- End: Intro -->

<!-- Start: Personal profile -->
<section id="profile" class="section">
    <div class="container">
        <div class="row">
            <div class="col-full">
                <h2 class="section-title">Welcome to aero-projects development</h2>
                <div class="centered line"></div>
            </div>
        </div>

        <div class="row section-content wel-sec">
            <div class="col-1-3">
                <h4>Profile info</h4>

                <p>My name: <%=request.getSession().getAttribute("userName")%></p>
                <p>My status: <%=request.getSession().getAttribute("userStatus")%></p>
                <p>My e-mail: <%=request.getSession().getAttribute("userEmail")%></p>
                <p><a href="/projects" class="button button-medium button-submit">My projects</a></p>
            </div>

        </div>
    </div>
</section>
<!-- End: Personal profile -->

<footer>
    <div class="container">
        2018 created by Danil Yusupov for educational purposes.
    </div>
</footer>

</body>
</html>
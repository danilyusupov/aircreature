<!DOCTYPE html>
<%@ page pageEncoding="UTF-8" %>
<html>

<head>

    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <title>Air Creature</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Loading: Normalize, Grid and Styles -->
    <link rel="stylesheet" href="assets/css/normalize.css" media="screen">
    <link rel="stylesheet" href="assets/css/grid.css" media="screen">
    <link rel="stylesheet" href="assets/css/style-home.css" media="screen">

    <!-- Loading: Font Awesome -->
    <link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css">
    <!--[if IE 7]>
    <link rel="stylesheet" href="assets/font-awesome/css/font-awesome-ie7.min.css">
    <![endif]-->

    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <style>
   body {
    background-image: url("assets/img/my-background.jpg");
    background-size: cover;
   }
  </style>
</head>

<body>

    <% if (request.getSession().getAttribute("errorMessage") != null) { %>
    <div class="fix">
        <p align="center"><h3><font color="red"><%=request.getSession().getAttribute("errorMessage")%></font></h3></p>
     </div
     <% } %>
     <% if (request.getSession().getAttribute("success") != null) { %>
     <div class="fix">
        <p align="center"><h3><font color="green"><%=request.getSession().getAttribute("success")%></font></h3></p>
     </div>
     <% } %>

<!-- Start: Intro -->
<section id="intro" class="fx-backstretch">
    <div class="info">
        <div class="container">
            <div class="row">
                <div class="col-full"><h1>Air Creature</h1></div>
            </div>
            <div class="row">
                <div class="col-1-4 centered line"></div>
            </div>
            <div class="row">
                <div class="col-full"><h4>Inventing future of flights...</h4></div>
            </div>
        </div>
    </div>

    <nav id="nav">
        <ul class="clearfix">
            <li><a href="/personal_area">Personal Area</a></li>
            <li><a href="/registration">Registration</a></li>
            <li><a href="/companies">Companies</a></li>
            <li><a href="/contact">Contact</a></li>
            <% if (request.getSession().getAttribute("userName") != null) {
                %><li><a href="/logout">Logout (
                                                <%=request.getSession().getAttribute("userName")%>
                                                )</a></li>
                <% } %>
        </ul>
    </nav>
</section>
<!-- End: Intro -->

<!-- Start: Personal profile -->
<section id="profile" class="section">
    <div class="container">
        <div class="row">
            <div class="col-full">
                <h2 class="section-title">Welcome</h2>
                <div class="centered line"></div>
            </div>
        </div>

        <div class="row section-content wel-sec">
            <p>This is a service, where You can put Your inventions & ideas in aircraft
                building
                and perhaps some big corporations will notice Your project & will offer a contract
                to You.</p>
        </div>
    </div>
</section>
<!-- End: Personal profile -->

<!-- Start: Contact -->
<section id="contact" class="section">
    <div class="container">

        <div class="row">
            <div class="col-full">
                <h2 class="section-title">Log in</h2>
                <div class="centered line"></div>
            </div>
        </div>

        <div class="row section-content">
            <div class="col-1-1 col-wrap centered text-center">
                <div id="form-message"></div>
                <form id="form-contact" name="form-contact" method="post">
                    <div class="col-full">
                        <label for="userName">Your nickname</label>
                        <input type="text" name="userName" id="userName">
                    </div>
                    <div class="col-full">
                        <label for="userPassword">Your password</label>
                        <input type="password" name="userPassword" id="userPassword">
                    </div>
                    <div class="col-full">
                        <input type="submit" class="button button-medium button-submit" id="submit-contact" value="LOGIN">
                    </div>
                </form>
            </div>

            <div class="separator"></div>


        </div>
    </div>
</section>

<footer>
    <div class="container">
        2018 created by Danil Yusupov for educational purposes.
    </div>
</footer>

</body>

</html>
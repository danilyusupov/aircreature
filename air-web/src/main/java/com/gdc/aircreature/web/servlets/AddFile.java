package com.gdc.aircreature.web.servlets;

import com.gdc.aircreature.service.dao.FileDaoManager;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.*;
import java.io.IOException;

/**
 * This servlet provides creating {@code File} and saving them to connected database.
 *
 * @author YusupovD
 */

@MultipartConfig
public class AddFile extends HttpServlet implements ServiceLogger {

    private FileDaoManager fileManager = new FileDaoManager();

    public void init(ServletConfig config) {
        log.info(config.getServletName() + " servlet invoked");
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("/add_file.jsp").forward(request, response);
        request.getSession().removeAttribute("fileErrorMessage");
        log.info(getClass().toString() + ": Entered page '/add_file'");
    }

    /**
     * Creates & adds {@code File} to connected database according to received {@code fileName} & {@code fileRef} into project
     * with {@code projectName}. (1) If one or both of received fields are empty, servlet redirects client to "/add_file"
     * with {@code fileErrorMessage} session attribute. (2) If connected database does not exists {@code File} with received
     * {@code fileName} in current {@code projectName}, then servlet saves created {@code File} to database & redirects
     * client to "/view_project". (3) Else servlet redirects client to "/add_file" with {@code fileErrorMessage} in
     * cause of existence of {@code File} with entered {@code fileName}.
     *
     * @param request  - simple request
     * @param response - simple response
     * @throws IOException if servlet can't find page in path "/add_file" & "view_project".
     */
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        String projectName = (String) request.getSession().getAttribute("projectName");

        try {
            String fileName = request.getParameter("fileName");
            String fileRef = request.getParameter("fileRef");
            String message = fileManager.saveFile(fileName, fileRef, projectName);
            request.setAttribute("fileMessage", message);
            log.info(getClass().toString() + ":Received link to file:\n" +
                    fileName + " | " + fileRef);
            response.sendRedirect("/view_project");
        } catch (NullPointerException e){
            Part fileData = request.getPart("fileData");
            String name = fileData.getSubmittedFileName();
            String type = fileData.getContentType();
            String message = fileManager.saveFile(name, fileData, type, projectName);
            log.info(getClass().toString() + ":Received file in upload: '" + name + "' | " + type);
            request.setAttribute("fileMessage", message);
            response.sendRedirect("/view_project");
        }
    }

    public void destroy() {
        log.info("AddFile servlet destroyed");
    }

}

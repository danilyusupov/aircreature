package com.gdc.aircreature.web.servlets;

import org.apache.log4j.Logger;

/**
 * Provides {@code Logger} for all implementing servlets. Made in cause of avoiding code copying.
 *
 * @author Yusupov Danil
 */
public interface ServiceLogger {
    Logger log = Logger.getLogger(ServiceLogger.class);
}

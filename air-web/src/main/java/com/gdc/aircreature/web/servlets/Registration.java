package com.gdc.aircreature.web.servlets;

import com.gdc.aircreature.dao.hsqldb.HsqldbUserDao;
import com.gdc.aircreature.model.Status;
import com.gdc.aircreature.model.User;
import com.gdc.aircreature.service.dao.UserDaoManager;
import com.gdc.aircreature.service.hashing.Hashing;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * This servlet creates {@code User} according to inputted params & saves it into connected database.
 *
 * @author YusupovD
 */
public class Registration extends HttpServlet implements ServiceLogger{

    private HsqldbUserDao userDao = new HsqldbUserDao();

    public void init(ServletConfig config) {
        log.info(config.getServletName() + " servlet invoked");
    }

    /**
     * While pushing "/registration.jsp" to client this method also clears {@code registrationErrorMessage} &
     * {@code registrationSuccess} session attributes to avoid permanent presence of log messages in page.
     *
     * @param request - simple request.
     * @param response - simple response.
     * @throws IOException if servlet can't find file "/registration.jsp".
     */
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("/registration.jsp").forward(request, response);
        request.getSession().removeAttribute("registrationErrorMessage");
        request.getSession().removeAttribute("registrationSuccess");
        log.info(getClass().toString() + ": Entered page '/registration'");
    }

    /**
     * (1) Creates & saves {@code User} into connected database if database does not exists {@code User} with such
     * {@code userName}. (2) If {@code userName} or {@code userPassword} or {@code userEmail} is empty, servlet
     * redirects client to "/registration" page with {@code registrationErrorMessage} session attribute. (3) Else
     * servlet redirects client to "/registration" page with {@code registrationErrorMessage} session attribute in cause
     * of
     *
     * {@code userStatus} sets by default as {@code Status.Developer}.
     *
     * @param request
     * @param response
     * @throws IOException
     */
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String userName = request.getParameter("userName");
        String userPassword = request.getParameter("userPassword");
        String userEmail = request.getParameter("userEmail");
        if (userName.equals("") || userPassword.equals("") || userEmail.equals("")) {
            request.getSession().setAttribute("registrationErrorMessage", "Please fill all fields.");
            log.warn(getClass().toString() + ": Empty fields for registration page received.");
            response.sendRedirect("/registration");
            return;
        }
        if (userDao.get(userName) == null) {
            userDao.save(new User(userName, Hashing.hash(userPassword), Status.Developer, userEmail));
            request.getSession().setAttribute("registrationSuccess", "Registration successful, " + userName +
                    ". Now You can log-in.");
            log.info(getClass().toString() + ": Registration success for user '" + userName + "' with password: '" + userPassword +
                    "' and Email: '" + userEmail + "'.");
            response.sendRedirect("/registration");
        } else {
            request.getSession().setAttribute("registrationErrorMessage", "This name is already used! Try another.");
            log.warn(getClass().toString() + ": '" + userName + "' is already exists in database. Canceling registration.");
            response.sendRedirect("/registration");
        }
    }

    public void destroy() {
        log.info("Registration servlet destroyed");
    }
}

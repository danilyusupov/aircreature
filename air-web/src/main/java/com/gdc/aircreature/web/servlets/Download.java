package com.gdc.aircreature.web.servlets;

import com.gdc.aircreature.dao.hsqldb.HsqldbFileDaoBeta;
import com.gdc.aircreature.model.File;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedOutputStream;
import java.io.IOException;

public class Download extends HttpServlet implements ServiceLogger {

    private HsqldbFileDaoBeta fileDaoBeta = new HsqldbFileDaoBeta();

    public void init(ServletConfig config) {
        log.info(config.getServletName() + " servlet invoked");
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        Long fileId = (Long) request.getSession().getAttribute("fileId");
        File file = fileDaoBeta.get(fileId);
        String fileName = file.getFileName();
        String fileType = file.getContentType();
        String fileExtension = file.getFileExtension();
        response.setContentType(fileType);
        response.setHeader("Content-disposition", "attachment; filename=" + fileName);
        try (BufferedOutputStream out = new BufferedOutputStream(response.getOutputStream())){
            out.write(file.getFileData());
        }
        log.info(getClass().toString() + ":Upped file to download:\n" +
                fileName + " | ext: " + fileExtension + " | type:" + fileType);
    }
}

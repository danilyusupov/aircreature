package com.gdc.aircreature.web.servlets;

import com.gdc.aircreature.dao.hsqldb.HsqldbUserDao;
import com.gdc.aircreature.model.User;
import com.gdc.aircreature.service.dao.UserDaoManager;
import com.gdc.aircreature.service.hashing.Hashing;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * This servlet provides homepage of service, where client can login.
 *
 * @author YusupovD
 */
public class Home extends HttpServlet implements ServiceLogger{

    private HsqldbUserDao userDao = new HsqldbUserDao();

    public void init(ServletConfig config) {
        log.info(config.getServletName() + " servlet invoked");
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("/home.jsp").forward(request, response);
        request.getSession().removeAttribute("errorMessage");
        request.getSession().removeAttribute("success");
        log.info(getClass().toString() + ": Entered page '/home'");
    }

    /**
     * Logs in client by {@code userName} & {@code userPassword}. (1) If one or both of this field are empty, servlet
     * redirects client to "/home" with session attribute {@code errorMessage}. (2) If connected database does not
     * exists {@code User}, then servlet redirects client to "/home" with session attribute {@code errorMessage}. (3)
     * If hashed {@code userPassword} matches with appropriate existent one, servlet redirects client to "/home" with
     * session attributes {@code success}, {@code userId}, {@code userName}, {@code userStatus} & {@code userEmail}.
     * (4) Else servlet redirects client to "/home" with session attribute {@code errorMessage} in cause of invalid
     * password.
     *
     * @param request - simple request
     * @param response - simple response
     * @throws IOException if servlet can't find page in path "/home".
     */
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String userName = request.getParameter("userName");
        String userPassword = request.getParameter("userPassword");
        if (userName.equals("") || userPassword.equals("")) {
            request.getSession().setAttribute("errorMessage", "Some fields are empty! Try again.");
            log.warn(getClass().toString() + ": Empty fields in login form.");
            response.sendRedirect("/home");
            return;
        }
        if (userDao.get(userName) == null){
            request.getSession().setAttribute("errorMessage", "Sorry, no such account. Register please.");
            log.warn(getClass().toString() + ": Cannot find user: '" + userName + "'.");
            response.sendRedirect("/home");
            return;
        }
        User user = userDao.get(userName);
        if (user.getUserPassword().matches(Hashing.hash(userPassword))) {
            HttpSession session = request.getSession();
            session.setAttribute("userId", user.getUserId());
            session.setAttribute("userName", user.getUserName());
            session.setAttribute("userStatus", user.getUserStatus());
            session.setAttribute("userEmail", user.getUserEmail());
            session.setAttribute("success", "Logged in successful");
            log.info(getClass().toString() + ": '" + user.getUserName() + "' logged in successful with userId: " + user.getUserId() +
                    ", userStatus: '" + user.getUserStatus().toString() + "' and userEmail: '" + user.getUserEmail() +
                    "'.");
            response.sendRedirect("/home");
        } else {
            request.getSession().setAttribute("errorMessage", "Invalid password!");
            log.warn(getClass().toString() + ": Invalid password for user: '" + userName + "'.");
            response.sendRedirect("/home");
        }
    }

    public void destroy() {
        log.info("Home servlet destroyed");
    }
}

package com.gdc.aircreature.web.servlets;

import com.gdc.aircreature.dao.hsqldb.HsqldbProjectDao;
import com.gdc.aircreature.model.Project;
import com.gdc.aircreature.service.dao.ProjectDaoManager;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

/**
 * This servlet provides list of {@code Project} to client. List contains only projects created by current {@code User}.
 *
 * @author YusupovD
 */
public class Projects extends HttpServlet implements ServiceLogger{

    private HsqldbProjectDao projectDao = new HsqldbProjectDao();

    public void init(ServletConfig config) {
        log.info(config.getServletName() + " servlet invoked");
    }


    /**
     * Gets {@code List} of {@code Project} owned by client with {@code userId} and pushes it to requested "/projects"
     * page.
     *
     * @param request - simple request.
     * @param response - simple response.
     * @throws ServletException if servlet cant push file "/projects.jsp" to client.
     * @throws IOException if servlet can't find file "/projects.jsp".
     */
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        request.getSession().removeAttribute("projectName");
        Long userId = (Long) session.getAttribute("userId");
        List<Project> list = projectDao.getByAuthor(userId);
        Project[] projectList = new Project[list.size()];
        for (int i = 0; i < list.size(); i++) {
            projectList[i] = list.get(i);
        }
        session.setAttribute("projectList", projectList);
        request.getRequestDispatcher("/projects.jsp").forward(request, response);
        request.getSession().removeAttribute("projectSuccess");
        log.info(getClass().toString() + ": User with Id: " + userId + ", received his project page successful.");
    }

    /**
     * Redirects client to "/view_page" with session attribute of {@code projectName}, which it received from
     * {@code select} parameter by POST request from page "/projects".
     *
     * @param request - simple request.
     * @param response - simple response.
     * @throws IOException if servlet can't redirect to "/view_project".
     */
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String projectName = request.getParameter("select");
        request.getSession().setAttribute("projectName", projectName);
        log.info(getClass().toString() + ": Entering page of projectName: '" + projectName + "'.");
        response.sendRedirect("/view_project");
    }

    public void destroy() {
        log.info("Projects servlet destroyed");
    }
}

package com.gdc.aircreature.web.servlets;

import javax.servlet.ServletConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * This servlet provides only 'logout' for client by ending his session.
 *
 * @author YusupovD
 */
public class Logout extends HttpServlet implements ServiceLogger{

    public void init(ServletConfig config) {
        log.info("Starting " + config.getServletName() + " servlet");
    }

    /**
     * This method invalidates current clients session if it's running & redirects him to "/home" page.
     *
     * @param request  comes from client and indicates, that he wants to log-out
     * @param response provides redirecting client to "/" path
     * @throws IOException      if there will be some problems with file "home.jsp" input
     */

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        if (request.getSession().getAttribute("userName") != null) {
            String userName = (String) request.getSession().getAttribute("userName");
            request.getSession().removeAttribute("userName");
            request.getSession().removeAttribute("userId");
            request.getSession().removeAttribute("userStatus");
            request.getSession().removeAttribute("userEmail");
            request.getSession().invalidate();
            request.getSession().setAttribute("success", "Logout successful");
            response.sendRedirect("/home");
            log.info(getClass().toString() + ": '" + userName + "' logged out successful.");
            return;
        }
        response.sendRedirect("/home");
    }

    public void destroy() {
        log.info("Servlet on way '/logout' destroyed...");
    }
}

package com.gdc.aircreature.web.servlets;

import com.gdc.aircreature.dao.hsqldb.HsqldbProjectDao;
import com.gdc.aircreature.model.Project;
import com.gdc.aircreature.model.ProjectType;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * This servlet provides creating {@code Project} entity & saving it into connected database.
 *
 * @author YusupovD
 */
public class CreateProject extends HttpServlet implements ServiceLogger{

    private HsqldbProjectDao projectDao = new HsqldbProjectDao();

    public void init(ServletConfig config) {
        log.info(config.getServletName() + " servlet invoked");
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("/create_project.jsp").forward(request, response);
        request.getSession().removeAttribute("projectErrorMessage");
        log.info(getClass().toString() + ": Entered page '/create_project'");
    }

    /**
     * Creates & adds {@code Project} to connected database according to received {@code projectName}, {@code userId} &
     * {@code projectType}. (1) If one or all of received fields are empty, servlet redirects client to "/createProject"
     * with {@code projectErrorMessage} session attribute. (2) If connected database does not exists {@code Project}
     * with received {@code projectName}, then servlet saves created {@code Project} to database & redirects client to
     * "/projects". (3) Else servlet redirects client to "/createProject" with {@code projectErrorMessage} in cause of
     * existence of {@code Project} with entered {@code projectName}.
     *
     * @param request - simple request
     * @param response - simple response
     * @throws IOException if servlet can't find page in path "/createProject" & "projects".
     */
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        HttpSession session = request.getSession();
        String projectName = request.getParameter("projectName");
        Long projectAuthor = (Long) session.getAttribute("userId");
        ProjectType projectType = ProjectType.valueOf(request.getParameter("projectType"));
        String description = request.getParameter("description");
        log.info(getClass().toString() + ": Receiving for creating project: projectName: '" + projectName + "' authorId: " + projectAuthor +
                " projectType: '" + projectType + "'.");
        if (projectName.equals("")) {
            request.getSession().setAttribute("projectErrorMessage", "Please fill all fields.");
            log.warn(getClass().toString() + ": Entered empty name for new project. UserId = " + projectAuthor);
            response.sendRedirect("/createProject");
            return;
        }
        if (projectDao.get(projectName) == null) {
            Project project = new Project(projectName, projectAuthor, projectType, description);
            Long projectId = projectDao.save(project);
            request.getSession().setAttribute("projectSuccess", "Created project: '" + projectName +
                    "'");
            log.info(getClass().toString() + ": Created project '" + projectName + "' by authorId: " + projectAuthor + ", with projectType: '"
                    + projectType + "'.");
            response.sendRedirect("/projects");
        } else {
            request.getSession().setAttribute("projectErrorMessage", "This name is already used! Try another.");
            log.warn(getClass().toString() + ": Project '" + projectName + "' already exists. UserId: " + projectAuthor + ".");
            response.sendRedirect("/createProject");
        }

    }

    public void destroy() {
        log.info("CreateProject servlet destroyed");
    }

}

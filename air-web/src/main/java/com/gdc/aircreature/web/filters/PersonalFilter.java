package com.gdc.aircreature.web.filters;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * This filter sorts authorized users & not. Those users, who authorized receiving access to filtered servlets.
 * List of filtered servlets You can find in "WEB-INF/web.xml".
 *
 * @author YusupovD
 */
public class PersonalFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse res = (HttpServletResponse) response;

        if (req.getSession().getAttribute("userName") == null) {
            req.getSession().setAttribute("errorMessage", "You are not logged in... Login please.");
            res.sendRedirect("/home");
        } else {
            chain.doFilter(req, res);
        }
    }

    @Override
    public void destroy() {

    }
}

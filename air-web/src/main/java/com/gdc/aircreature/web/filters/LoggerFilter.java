package com.gdc.aircreature.web.filters;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Simple filter, which logs servlets. List of logging servlets You can see in "WEB-INF/web.xml".
 *
 * @author YusupovD
 */
public class LoggerFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    /**
     * Also while filtering there is character encoding to "UTF-8" realised.
     */
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        request.setCharacterEncoding("UTF-8");

        System.out.println(request.getMethod() + "   " + request.getRequestURL() + "   " + response.getStatus());
        filterChain.doFilter(request, response);
    }

    @Override
    public void destroy() {

    }
}

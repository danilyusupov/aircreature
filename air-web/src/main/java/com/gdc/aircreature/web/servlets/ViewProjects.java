package com.gdc.aircreature.web.servlets;

import com.gdc.aircreature.dao.hsqldb.HsqldbFileDaoBeta;
import com.gdc.aircreature.dao.hsqldb.HsqldbProjectDao;
import com.gdc.aircreature.dao.hsqldb.HsqldbUserDao;
import com.gdc.aircreature.model.File;
import com.gdc.aircreature.model.Project;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

public class ViewProjects extends HttpServlet implements ServiceLogger {

    private HsqldbProjectDao projectDao = new HsqldbProjectDao();
    private HsqldbUserDao userDao = new HsqldbUserDao();
    private HsqldbFileDaoBeta fileDao = new HsqldbFileDaoBeta();


    public void init(ServletConfig config) {
        log.info(config.getServletName() + " servlet invoked");
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        String projectName = (String) session.getAttribute("projectName");
        Project project = projectDao.get(projectName);
        List<File> list = fileDao.getAll(project.getProjectId());
        File[] fileList = new File[list.size()];
        for (int i = 0; i < list.size(); i++) {
            fileList[i] = list.get(i);
        }
        session.setAttribute("fileList", fileList);
        session.setAttribute("projectName", projectName);
        session.setAttribute("projectAuthor", userDao.get(project.getProjectAuthor()).getUserName());
        session.setAttribute("projectType", project.getProjectType().toString());
        session.setAttribute("description", project.getDescription());
        log.info(getClass().toString() + ": For viewing project received: projectName: '" + projectName +
                "', authorId: " + userDao.get(project.getProjectAuthor()).getUserName() + ", projectType: '" +
                project.getProjectType().toString() + "'.");
        request.getRequestDispatcher("/view_project.jsp").forward(request, response);
        log.info(getClass().toString() + ": Entered page '/view_project'");
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String fileName = request.getParameter("select");
        String projectName = (String) request.getSession().getAttribute("projectName");
        log.warn(getClass().toString() + ": Received projectName: '" + projectName + "', Selected file to delete: '" + fileName + "'.");
        Project project = projectDao.get(projectName);
        fileDao.delete(fileDao.get(project.getProjectId(), fileName).getFileId());
        log.info(getClass().toString() + ": Deleted file: '" + fileName + "' from project '" + projectName + "'.");
        response.sendRedirect("/view_project");
    }

    public void destroy() {
        log.info("ViewProjects servlet destroyed");
    }

}

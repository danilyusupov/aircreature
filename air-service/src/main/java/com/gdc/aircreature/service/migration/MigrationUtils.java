package com.gdc.aircreature.service.migration;

import com.gdc.aircreature.dao.hsqldb.DatabaseConnection;
import com.gdc.aircreature.service.dao.MigrationDaoManager;
import com.gdc.aircreature.service.exception.DaoServiceException;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.*;
import java.util.Properties;

import static com.gdc.aircreature.service.dao.DaoManager.isExistent;

public class MigrationUtils {

    protected static final Logger log = Logger.getLogger(MigrationUtils.class);
    private final String folderPath;
    private final String propPath = "/migration/";
    private final MigrationDaoManager migrationDao;
    private String tableName;

    public MigrationUtils(String folderPath) {
        this.folderPath = folderPath;
        this.migrationDao = new MigrationDaoManager();
        this.tableName = migrationDao.getTableName();
    }

    public MigrationUtils(String folderPath, String tableName) {
        this.folderPath = folderPath;
        this.migrationDao = new MigrationDaoManager(tableName);
        this.tableName = tableName;
    }

    /**
     * Checks current version of database
     *
     * @return database version
     */
    public int checkVersion() {
        if (!isExistent(migrationDao.getTableName())) {
            return 0;
        }
        try (Connection connection = DatabaseConnection.connect()) {
            Statement statement = connection.createStatement();
            ResultSet result = statement.executeQuery("SELECT MAX(Version) FROM " +
                    tableName + ";");
            if (result.next()){
                String version = result.getString("C1");
                return (version == null) ? 0 : Integer.valueOf(version);
            }
            throw new DaoServiceException("Error getting version from table '" + tableName + "'");
        } catch (SQLException e) {
            throw new DaoServiceException("Error checking database version", e);
        }
    }

    /**
     * @param version minor database version
     * @return filename for updating
     */
    public String getFileName(String version) {
        File folder = new File(folderPath);
        String[] folderList = folder.list();
        for (int i = 0; i < folderList.length; i++) {
            String prefix = folderList[i].substring(1, 3);
            if (prefix.matches(version)) {
                return folderList[i];
            }
        }
        return null;
    }

    public String getQuery(String version) {
        Properties properties = new Properties();
        String filePath = propPath + getFileName(version);
        try {
            properties.load(MigrationManager.class.getResourceAsStream(filePath));
            return properties.getProperty("sql.query");
        } catch (IOException e) {
            throw new DaoServiceException("Error getting sql query from " + filePath +
                    ".properties file", e);
        }
    }

    /**
     * Gets latest available version for migrating according to files in "/migration" folder.
     * @return number of latest available version
     * @throws NullPointerException if folder path is invalid or there is no files there.
     */
    int getLastVersion() {
        int lastVersion = 0;
        File folder = new File(folderPath);
        String[] fileList = folder.list();
        for (String fileName : fileList) {
            int version = Integer.valueOf(fileName.substring(1, 3));
            if (version > lastVersion) {
                lastVersion = version;
            }
        }
        return lastVersion;
    }

    public void writeHistoryNote(int targetVersion) {
        String version = (String.valueOf(targetVersion).length() == 1) ? "0" + targetVersion
                : String.valueOf(targetVersion);
        String fileName = getFileName(version);
        String comment = fileName.substring(4).split("\\.")[0];
        try (Connection connection = DatabaseConnection.connect()) {
            PreparedStatement statement = connection.prepareStatement(
                    "INSERT INTO " + tableName + " VALUES(null, ?, ?);");
            statement.setString(1, version);
            statement.setString(2, comment);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoServiceException("Error writing migration history for file '" + fileName + "'", e);
        }
    }

    public MigrationDaoManager getMigrationDao() {
        return migrationDao;
    }
}

package com.gdc.aircreature.service.migration;

public class DbGeneration {
    public static void main(String[] args) {
        MigrationManager manager = new MigrationManager(
                new MigrationUtils(System.getenv("AIR_PROJECT_HOME") +
                        "\\air-service\\src\\main\\resources\\migration"));
        manager.updateDatabase();
    }
}

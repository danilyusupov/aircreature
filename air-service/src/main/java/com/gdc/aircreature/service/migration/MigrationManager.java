package com.gdc.aircreature.service.migration;

import com.gdc.aircreature.dao.hsqldb.DatabaseConnection;
import com.gdc.aircreature.service.dao.MigrationDaoManager;
import com.gdc.aircreature.service.dao.ProjectDaoManager;
import com.gdc.aircreature.service.dao.UserDaoManager;
import com.gdc.aircreature.service.exception.DaoServiceException;
import org.apache.log4j.Logger;

import java.sql.*;

import static com.gdc.aircreature.service.dao.DaoManager.isExistent;

/**
 * This is a manager for database migration.
 * [ WARN ] : files for migration must lay in folder '/src/main/resources/migration/'
 * [ WARN ] : migration file extension is '.properties'.
 * [ WARN ] : filename must starts with upper cased letter 'V'. Then after letter stays
 * update version, which consists from two characters, for example: '01'. After version
 * across symbol '_' stays comment, where words divided by '-'. Example of filename:
 * 'V01_test-migration.properties'.
 * [ WARN ] : in this file sql query writes as a variable of parameter 'sql.query', for
 * example: 'sql.query=DROP TABLE TEST_MIGR;'.
 */
public class MigrationManager {

    protected static final Logger log = Logger.getLogger(MigrationManager.class);
    private final ProjectDaoManager projectDaoManager = new ProjectDaoManager();
    private final UserDaoManager userDaoManager = new UserDaoManager();
    private final MigrationDaoManager migrationDao = new MigrationDaoManager();
    private final MigrationUtils utils;

    public MigrationManager(MigrationUtils utils) {
        this.utils = utils;
    }

    /**
     * Updates all tables in database. If there is no needed tables - it creates them.
     */
    public void updateDatabase() {
        int currentVersion = utils.checkVersion();
        int lastVersion = utils.getLastVersion();
        int diff = lastVersion - currentVersion;

        if (currentVersion == 0){
            checkCreateAllTables();
            for (int i = 0; i < lastVersion; i++) {
                int targetVersion = currentVersion + 1 + i;
                log.info("Database updated to version " + updateStep(targetVersion));
            }
        } else {
            for (int i = 0; i < diff; i++) {
                int targetVersion = currentVersion + 1 + i;
                log.info("Database updated to version " + updateStep(targetVersion));
            }
        }
    }

    /**
     * Updates database once from current version.
     *
     * @throws DaoServiceException during execution sql query
     */
    public String updateStep(int targetVersion) {
        String version = (String.valueOf(targetVersion).length() == 1) ? "0" + String.valueOf(targetVersion)
                : String.valueOf(targetVersion);
        String filePrefix = "V" + version;
        try (Connection connection = DatabaseConnection.connect()) {
            Statement statement = connection.createStatement();
            statement.executeUpdate(utils.getQuery(version));
        } catch (SQLException e) {
            throw new DaoServiceException("Error in updating step with file: '" + filePrefix + "'", e);
        }
        utils.writeHistoryNote(targetVersion);
        log.warn("Database updated to version " + version);
        return version;
    }



    /**
     * Checks existence of all needed tables in project and creates them if necessary
     *
     * @return {@code true} if even one of this tables was created
     */
    public boolean checkCreateAllTables() {
        boolean modified = false;
        if (!isExistent(migrationDao.getTableName())) {
            migrationDao.createTable();
            log.info("Created '" + migrationDao.getTableName() + "' table.");
            modified = true;
        }
        if (!isExistent(userDaoManager.getUserDao().getTableName())) {
            userDaoManager.createTable();
            log.info("Created '" + userDaoManager.getUserDao().getTableName() + "' table.");
            modified = true;
        }
        if (!isExistent(projectDaoManager.getProjectDao().getTableName())) {
            projectDaoManager.createTable();
            log.info("Created '" + projectDaoManager.getProjectDao().getTableName() + "' table.");
            modified = true;
        }
        return modified;
    }
}

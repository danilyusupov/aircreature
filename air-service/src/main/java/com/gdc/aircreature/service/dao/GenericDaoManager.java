package com.gdc.aircreature.service.dao;

/**
 * General interface for all DAO managers.
 *
 * @author Yusupov Danil
 */
public interface GenericDaoManager {

    /**
     * Creates table in connected database with special name & structure.
     *
     * @param tableName name of new table
     * @return {@code true} if table was created or {@code false} if table wasn't created
     */
    boolean createTable(String tableName);

    /**
     * Removes required table from database with all existent data.
     *
     * @param tableName name of table to be removed
     * @return {@code true} if table was removed or {@code false} if table wasn't removed.
     */
    boolean deleteTable(String tableName);
}

package com.gdc.aircreature.service.dao;

import com.gdc.aircreature.dao.hsqldb.DatabaseConnection;
import com.gdc.aircreature.service.exception.DaoServiceException;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Set;

/**
 * Generic class for all DAO managers, which needs to avoid code copying.
 *
 * @author Yusupov Danil
 */
public abstract class DaoManager implements GenericDaoManager {

    /**
     * Logger for all inherited DAO manager classes. No need to initiate it again.
     */
    protected static final Logger log = Logger.getLogger(DaoManager.class);

    /**
     * Getting all table names existent in connected database.
     *
     * @return {@code Set} of all table names existent in database. If no one tables exists in database - returns
     * {@code Set} with zero size.
     * @throws DaoServiceException in cause of SQL error
     */
    static Set<String> getTables() {
        Set<String> tableNamesSet = new HashSet<>();

        try (Connection connection = DatabaseConnection.connect()) {

            Statement statement = connection.createStatement();
            ResultSet result = statement.executeQuery("SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE " +
                    "TABLE_TYPE='BASE TABLE' AND TABLE_SCHEMA='PUBLIC';");

            while (result.next()) {
                tableNamesSet.add(result.getString("TABLE_NAME"));
            }
            return tableNamesSet;

        } catch (SQLException e) {
            throw new DaoServiceException("Error trying to get set of table names", e);
        }
    }

    /**
     * Checks existence of requiring table.
     *
     * @param tableName name of table to check
     * @return {@code true} if table exists in database or {@code false} if table is not exists in database.
     */
    public static boolean isExistent(String tableName) {
        try{
            Set<String> tableNamesSet = getTables();
            return tableNamesSet.contains(tableName.toUpperCase());
        } catch (NullPointerException e){
            return false;
        }
    }

    /**
     * Inherited from {@code GenericDaoManager} interface & realized for all child classes. Removes table from database
     * with inserted name.
     *
     * @param tableName name of table to be removed
     * @return {@code true} if table was removed of {@code false} if table wasn't removed
     * @throws DaoServiceException in cause of SQL error.
     */
    @Override
    public boolean deleteTable(String tableName) {
        if (!isExistent(tableName)) {
            log.warn("You trying to delete nonexistent table '" + tableName + "'");
            return false;
        }
        try (Connection connection = DatabaseConnection.connect()) {
            Statement statement = connection.createStatement();
            return statement.executeUpdate("DROP TABLE " + tableName + ";") > 0;
        } catch (SQLException e) {
            throw new DaoServiceException("Deleting table '" + tableName + "' failed...", e);
        }
    }
}

package com.gdc.aircreature.service.hashing;

import com.gdc.aircreature.service.exception.DaoServiceException;

import javax.xml.bind.DatatypeConverter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Hashing {

    public static String hash(String target) {
        try{
            MessageDigest md = MessageDigest.getInstance("MD5");
                    md.update(target.getBytes());
            byte[] digest = md.digest();
            return DatatypeConverter.printHexBinary(digest).toUpperCase();
        } catch (NoSuchAlgorithmException e) {
            throw new DaoServiceException("Error trying to hash target string: ", e);
        }
    }

}

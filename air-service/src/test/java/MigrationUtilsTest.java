import com.gdc.aircreature.dao.hsqldb.DatabaseConnection;
import com.gdc.aircreature.service.migration.MigrationUtils;
import org.junit.Test;

import java.io.File;
import java.sql.*;

import static org.junit.Assert.assertEquals;

public class MigrationUtilsTest {

    /**
     * Check in 'resources/migration/' folder.
     */
    private final String fileName = "V01_test-migration.properties";
    private final String query = "DROP TABLE TEST_MIGR;";
    private final String comment = "test-migration";
    private String tableName = "TEST_MIGRATION";
    private final MigrationUtils utils = new MigrationUtils("src/main/resources/migration/", tableName);

    @Test
    public void testCheckVersion() {
        int n = 3;
        utils.getMigrationDao().createTable(tableName);
            try (Connection connection = DatabaseConnection.connect()) {
                for (int i = 1; i <= n; i++) {
                    PreparedStatement statement = connection.prepareStatement("INSERT INTO " +
                            tableName + " VALUES (null, ?, 'comment');");
                    statement.setString(1, String.valueOf(i));
                    statement.executeUpdate();
                }
                assertEquals(n, utils.checkVersion());
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                utils.getMigrationDao().deleteTable(tableName);
            }
        }

    @Test
    public void testGetFileName() {
        System.out.println(utils.getFileName("01"));
        assertEquals(fileName, utils.getFileName("01"));
    }

    @Test
    public void testGetQuery(){
        System.out.println(utils.getQuery("02"));
        assertEquals(query, utils.getQuery("02"));
    }

    @Test
    public void testWriteHistory(){
        utils.getMigrationDao().createTable(tableName);
        utils.writeHistoryNote(1);
        try (Connection connection = DatabaseConnection.connect()) {
                Statement statement = connection.createStatement();
                ResultSet result = statement.executeQuery("SELECT * FROM " +
                        tableName + " WHERE Version = '01';");
                while (result.next()){
                    assertEquals(comment, result.getString("Comment"));
                }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            utils.getMigrationDao().deleteTable(tableName);
        }
    }
}
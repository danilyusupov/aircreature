import com.gdc.aircreature.dao.hsqldb.DatabaseConnection;
import com.gdc.aircreature.service.dao.*;
import com.gdc.aircreature.service.migration.MigrationManager;
import com.gdc.aircreature.service.migration.MigrationUtils;
import org.junit.Test;

import java.sql.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


public class MigrationManagerTest {
    private final ProjectDaoManager projectDaoManager = new ProjectDaoManager();
    private final UserDaoManager userDaoManager = new UserDaoManager();
    private final MigrationDaoManager migrationDao = new MigrationDaoManager();
    private final TestTable testDao = new TestTable();
    private final String version = "02";
    private final String comment = "test-dropping";
    private String tableName = "TEST_MIGRATION";
    private final MigrationManager manager = new MigrationManager(
            new MigrationUtils("src/main/resources/migration/", tableName));

    @Test
    public void testCheckCreateAllTables() {
            manager.checkCreateAllTables();
            assertTrue(DaoManager.isExistent(projectDaoManager.getProjectDao().getTableName()));
            assertTrue(DaoManager.isExistent(userDaoManager.getUserDao().getTableName()));
            assertTrue(DaoManager.isExistent(migrationDao.getTableName()));
    }

    @Test
    public void testUpdateStep(){
        migrationDao.createTable(tableName);
        testDao.createTable(testDao.getTableName());
        assertEquals(version , manager.updateStep(2));
        try (Connection connection = DatabaseConnection.connect()) {
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM " +
                    tableName + " WHERE Version = ?;");
            statement.setString(1, version);
            ResultSet result = statement.executeQuery();
            while (result.next()){
                assertEquals(comment, result.getString("Comment"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            migrationDao.deleteTable(tableName);
        }
    }

    @Test
    public void testUpdateDatabase(){
//        testDao.createTable(testDao.getTableName());
//        manager.updateDatabase();
        //TODO: find way to test it without changing data in database...
    }
}

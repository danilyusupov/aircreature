import com.gdc.aircreature.service.dao.DaoManager;
import com.gdc.aircreature.service.dao.MigrationDaoManager;
import org.junit.Test;


import static org.junit.Assert.assertTrue;

public class MigrationDaoManagerTest {
    private MigrationDaoManager daoManager = new MigrationDaoManager();
    private final String tableName= "TEST_MIGRATION";

    @Test
    public void testCreateTable(){
        daoManager.createTable(tableName);
        assertTrue(DaoManager.isExistent(tableName));
        daoManager.deleteTable(tableName);
    }


}

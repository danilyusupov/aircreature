import com.gdc.aircreature.dao.hsqldb.HsqldbUserDao;
import com.gdc.aircreature.service.dao.DaoManager;
import com.gdc.aircreature.service.dao.UserDaoManager;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class UserDaoManagerTest {
    private final UserDaoManager daoManager = new UserDaoManager();
    private String tableName = "TEST_PROJECT";

    @Test
    public void testTableCreating(){
//        daoManager.createTable(tableName);
//        assertTrue(DaoManager.isExistent(tableName));
//        daoManager.deleteTable(tableName);
    }

    @Test
    public void testGetTableName(){
        assertEquals("users", daoManager.getUserDao().getTableName());
    }
}

import com.gdc.aircreature.dao.hsqldb.HsqldbFileDaoBeta;
import com.gdc.aircreature.model.File;
import org.junit.Test;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class FileDaoManagerTest {

    /**
     * Params to get file from DB into result path
     */
    private String resultPath = System.getenv("AIR_PROJECT_HOME") + "\\air-service\\src\\test\\resources\\";
    private String targetFile = "file.";
    private Long targetId = 13L;

    HsqldbFileDaoBeta dao = new HsqldbFileDaoBeta();

//    @Test
    public void getFileFromDB() throws IOException {
        File file = dao.get(targetId);
        try (BufferedOutputStream out = new BufferedOutputStream( new FileOutputStream(resultPath + targetFile + file.getFileExtension()))){
            int size = 0;
            byte[] data = file.getFileData();
            for (byte datum : data) {
                out.write(datum);
                size ++;
            }
            System.out.println("Size of received file is: " + size + " bytes\nContent type: " + file.getContentType());
        }
    }
}

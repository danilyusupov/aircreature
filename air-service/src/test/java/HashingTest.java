import com.gdc.aircreature.service.hashing.Hashing;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class HashingTest {
    private String password = "password";
    private String hash = "5F4DCC3B5AA765D61D8327DEB882CF99";

    @Test
    public void testHash(){
        assertEquals(hash, Hashing.hash(password));
    }
}
